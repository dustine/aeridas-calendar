import { AeriDate } from '$lib/calendars/AeriDate';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
	const year = +params.year ?? 9999;
	const month = +params.month ?? 1;
	const day = +params.day ?? 1;

	const date = new AeriDate(year, month, day);

	if (isNaN(date.time)) {
		throw error(400, 'Invalid date');
	}

	// const url = `/${year}/${month}/${day}.json`;
	// const res = await fetch(url);

	// if (res.ok) {
	// 	return {
	// 		props: {
	// 			date: date,
	// 			data: await res.json()
	// 		}
	// 	};
	// }

	return {
		date: date
	};
};

import { AeriDate } from '$lib/calendars/AeriDate';
import { error } from '@sveltejs/kit';

import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
	const year = +params.year ?? 9999;
	const date = new AeriDate(year, 1, 1);
	if (isNaN(date.time)) {
		throw error(400, 'Invalid date');
	}

	return {
		date: date
	};
};

import { CampaignsCalendars, CAMPAIGN_NAMES } from '$lib/calendars/CampaignsCalendars';
import { error } from '@sveltejs/kit';
import type { EntryGenerator, PageLoad } from './$types';

export const prerender = true;

export const entries: EntryGenerator = () => {
	return Object.keys(CAMPAIGN_NAMES).map((key) => ({
		slug: key
	}));
};

export const load: PageLoad = ({ params }) => {
	const slug = params?.slug as unknown as string;

	const campaign = CampaignsCalendars.get(slug);
	if (campaign) {
		return {
			initialEvents: campaign.sort((lst, rst) => lst.start.compare(rst.start)),
			slug: slug,
			name: CAMPAIGN_NAMES[slug]
		};
	}
	throw error(500, `Campaign not found: ${slug}`);
};

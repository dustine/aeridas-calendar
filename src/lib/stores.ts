import { browser } from '$app/environment';
import { AeriDate } from '$lib/calendars/AeriDate';
import { derived, writable } from 'svelte/store';
import { BirthdaysCalendar, EndlessBirthdaysCalendar } from './calendars/BirthdaysCalendar';
import type { CalendarEvent } from './calendars/CalendarEvent';
import { CAMPAIGNS, CampaignsCalendars } from './calendars/CampaignsCalendars';
import { EventsCalendar, UniqueEventsCalendar } from './calendars/EventsCalendar';
import type { OnceEvent } from './calendars/OnceEvent';

const slideMenu = writable(false);

const defaultDate = new AeriDate(
	[...CampaignsCalendars.values()]
		.flat()
		.reduce((prev, curr) =>
			!isNaN(curr.end.time) && curr.end.compare(prev.end) > 0 ? curr : prev
		).end
)
	.setSeconds(-1, true)
	.setHours(0)
	.setMinutes(0)
	.setSeconds(0);
let loadedDate = new AeriDate(
	browser ? +(window.localStorage.getItem('date') ?? defaultDate.time) : defaultDate
);
loadedDate = !isNaN(loadedDate.time) ? loadedDate : defaultDate;

const date = writable<AeriDate>(loadedDate);

function createYear() {
	const { subscribe, set, update } = writable(loadedDate.year);

	return {
		subscribe,
		set,
		increment: () => update((n) => n + 1),
		decrement: () => update((n) => n - 1)
	};
}
const year = createYear();

function createMonth() {
	const { subscribe, set, update } = writable(loadedDate.month);

	return {
		subscribe,
		set: (n: number) => set(((n - 1) % 5) + 1),
		increment: () =>
			update((n) => {
				if (n >= 5) {
					year.increment();
					return 1;
				} else {
					return n + 1;
				}
			}),
		decrement: () =>
			update((n) => {
				if (n <= 1) {
					year.decrement();
					return 5;
				} else {
					return n - 1;
				}
			})
	};
}
const month = createMonth();

date.subscribe((value: AeriDate) => {
	if (browser) {
		window.localStorage.setItem('date', `${value.time}`);
	}
	year.set(value.year);
	month.set(value.month);
});

const hasEvents = writable(true);
const hasRepeatable = writable(true);
const hasIntervals = writable(false);
const hasCampaigns = writable(true);
const whichCampaigns = writable(CAMPAIGNS);
const hasBirthdays = writable(true);
const hasDeceased = writable(true);

const events = derived(
	[hasEvents, hasRepeatable, hasCampaigns, whichCampaigns, hasBirthdays, hasDeceased],
	([$hasEvents, $hasRepeatable, $hasCampaigns, $whichCampaigns, $hasBirthdays, $hasDeceased]) =>
		new Array<CalendarEvent>().concat(
			$hasEvents ? ($hasRepeatable ? EventsCalendar : UniqueEventsCalendar) : [],
			$hasCampaigns
				? [...CampaignsCalendars.keys()]
						.filter((key) => $whichCampaigns.includes(key))
						.map((key) => CampaignsCalendars.get(key))
						.filter((value): value is OnceEvent[] => value !== undefined)
						.flat()
				: [],
			$hasBirthdays ? ($hasDeceased ? EndlessBirthdaysCalendar : BirthdaysCalendar) : []
		)
);
const monthEvents = derived([year, month, events], ([$year, $month, $events]) =>
	$events.filter((event) => event.in($year, $month))
);

export {
	slideMenu,
	date,
	year,
	month,
	events,
	monthEvents,
	hasEvents,
	hasRepeatable,
	hasIntervals,
	hasCampaigns,
	whichCampaigns,
	hasBirthdays,
	hasDeceased
};

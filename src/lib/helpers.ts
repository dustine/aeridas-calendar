import type { CalendarEvent } from './calendars/CalendarEvent';

export function uppercaseAll(s: string): string {
	return s
		.split(' ')
		.map((value) => value[0].toUpperCase() + value.slice(1).toLowerCase())
		.join(' ');
}

export function pluralize(s: string): string {
	return s + (s[s.length - 1].toLowerCase() == 's' ? "'" : "'s");
}

export function getMonthColor(month: number, target: 'text' | 'bg' | 'border' = 'text') {
	switch (month) {
		default:
			return '';
		case 1:
			switch (target) {
				default:
				case 'text':
					return 'text-yellow-600';
				case 'bg':
					return 'bg-yellow-600';
				case 'border':
					return 'border-yellow-600';
			}
		case 2:
			switch (target) {
				default:
				case 'text':
					return 'text-red-700';
				case 'bg':
					return 'bg-red-700';
				case 'border':
					return 'border-red-700';
			}
		case 3:
			switch (target) {
				default:
				case 'text':
					return 'text-zinc-500';
				case 'bg':
					return 'bg-zinc-500';
				case 'border':
					return 'border-zinc-500';
			}
		case 4:
			switch (target) {
				default:
				case 'text':
					return 'text-blue-800';
				case 'bg':
					return 'bg-blue-800';
				case 'border':
					return 'border-blue-800';
			}
		case 5:
			switch (target) {
				default:
				case 'text':
					return 'text-lime-800';
				case 'bg':
					return 'bg-lime-800';
				case 'border':
					return 'border-lime-800';
			}
	}
}

export function splitByDuration(events: CalendarEvent[], duration: number) {
	const yearMap = new Map<number, Map<number, Map<number, Map<number, CalendarEvent[]>>>>();
	const first = events[0]?.start?.time;
	if (!first) return yearMap;

	events.forEach((event) => {
		const year = event.start.year;
		const value = Math.floor((event.start.time - first) / duration);
		const month = event.fullYear ? 0 : event.start.month;
		const day = event.start.day;
		const durationMap =
			yearMap.get(year) ?? new Map<number, Map<number, Map<number, CalendarEvent[]>>>();
		const monthMap = durationMap.get(value) ?? new Map<number, Map<number, CalendarEvent[]>>();
		const dayMap = monthMap.get(month) ?? new Map<number, CalendarEvent[]>();
		const dayEvents: CalendarEvent[] = dayMap.get(day) ?? [];
		yearMap.set(
			year,
			durationMap.set(value, monthMap.set(month, dayMap.set(day, [...dayEvents, event])))
		);
	});
	return yearMap;
}

export function splitByYear(events: CalendarEvent[]) {
	const yearMap = new Map<number, Map<number, Map<number, CalendarEvent[]>>>();
	events.forEach((event) => {
		const year = event.start.year;
		const month = event.fullYear ? 0 : event.start.month;
		const day = event.start.day;
		const monthMap = yearMap.get(year) ?? new Map<number, Map<number, CalendarEvent[]>>();
		const dayMap = monthMap.get(month) ?? new Map<number, CalendarEvent[]>();
		const dayEvents: CalendarEvent[] = dayMap.get(day) ?? [];
		yearMap.set(year, monthMap.set(month, dayMap.set(day, [...dayEvents, event])));
	});
	return yearMap;
}

export function splitByPatron(events: CalendarEvent[]) {
	const patronMap = new Map<number, Map<number, Map<number, CalendarEvent[]>>>();
	events.forEach((event) => {
		const patron = event.start.patron;
		const month = event.fullYear ? 0 : event.start.month;
		const day = event.start.day;
		const monthMap = patronMap.get(patron) ?? new Map<number, Map<number, CalendarEvent[]>>();
		const dayMap = monthMap.get(month) ?? new Map<number, CalendarEvent[]>();
		const dayEvents: CalendarEvent[] = dayMap.get(day) ?? [];
		patronMap.set(patron, monthMap.set(month, dayMap.set(day, [...dayEvents, event])));
	});
	return patronMap;
}

export function splitByMonth(events: CalendarEvent[]) {
	const monthMap = new Map<number, Map<number, CalendarEvent[]>>();
	events.forEach((event) => {
		const month = event.fullYear ? 0 : event.start.month;
		const day = event.start.day;
		const dayMap = monthMap.get(month) ?? new Map<number, CalendarEvent[]>();
		const dayEvents: CalendarEvent[] = dayMap.get(day) ?? [];
		monthMap.set(month, dayMap.set(day, [...dayEvents, event]));
	});
	return monthMap;
}

export function splitByDay(events: CalendarEvent[]) {
	const dayMap = new Map<number, CalendarEvent[]>();
	events.forEach((event) => {
		const day = event.start.day;
		const dayEvents: CalendarEvent[] = dayMap.get(day) ?? [];
		dayMap.set(day, [...dayEvents, event]);
	});
	return dayMap;
}

import { matchersWithOptions } from 'jest-json-schema';
import { AeriDate } from './AeriDate';
import campaigns from './data/campaigns.json';
import characters from './data/characters.json';
import events from './data/events.json';

const isAeridate = (data: string) => !isNaN(new AeriDate(data).time);
const formats = {
	aeridate: isAeridate,
	'aeridate-repeat': (data: string) => isAeridate(data) || !isNaN(new AeriDate(data + ' 1').time)
};

const eventPropertiesSchema = {
	name: {
		type: 'string'
	},
	description: {
		type: 'string'
		// TODO: markdown verification
	},
	location: {
		type: 'string'
	}
};
const onceEventSchema = {
	type: 'object',
	properties: {
		...eventPropertiesSchema,
		start: {
			type: 'string',
			format: 'aeridate'
		},
		time: {
			type: 'string'
		},
		end: {
			type: 'string',
			format: 'aeridate'
		}
	},
	additionalProperties: false,
	required: ['name', 'start']
};
const repeatYearlyEventSchema = {
	type: 'object',
	properties: {
		...eventPropertiesSchema,
		repeat: {
			type: 'string',
			format: 'aeridate-repeat'
		},
		until: {
			type: 'string',
			format: 'aeridate'
		}
	},
	additionalProperties: false,
	required: ['name', 'repeat']
};

const eventsSchema = {
	type: 'array',
	items: {
		oneOf: [onceEventSchema, repeatYearlyEventSchema]
	}
};
const campaignsSchema = {
	type: 'object',
	patternProperties: {
		'^\\w+$': {
			type: 'array',
			items: {
				type: 'object',
				properties: {
					name: {
						type: 'string'
					},
					events: {
						type: 'array',
						items: onceEventSchema
					},
					required: ['name', 'events']
				}
			},
			additionalProperties: false
		}
	}
};
const charactersSchema = {
	type: 'array',
	items: {
		type: 'object',
		properties: {
			name: {
				type: 'string'
			},
			formerly: {
				type: 'string'
			},
			aka: {
				type: 'array',
				items: {
					type: 'string'
				}
			},

			start: {
				type: 'string',
				format: 'aeridate'
			},
			startKey: { oneOf: [{ type: 'string' }, { type: 'null' }] },
			end: {
				type: 'string',
				format: 'aeridate'
			},
			endKey: { oneOf: [{ type: 'string' }, { type: 'null' }] },
			days: {
				type: 'object',
				patternProperties: {
					'^\\w+$': {
						type: 'string',
						format: 'aeridate-repeat'
					}
				}
			},
			events: {
				type: 'object',
				patternProperties: {
					'^\\w+$': {
						type: 'string',
						format: 'aeridate'
					}
				}
			}
		},
		additionalProperties: false,
		required: ['name']
	}
};

expect.extend(matchersWithOptions({ formats }));

describe('Schemas', () => {
	expect(eventsSchema).toBeValidSchema();
	expect(campaignsSchema).toBeValidSchema();
	expect(charactersSchema).toBeValidSchema();
	test('events', () => {
		expect(events).toMatchSchema(eventsSchema);
	});
	test('campaigns', () => {
		expect(campaigns).toMatchSchema(campaignsSchema);
	});
	test('characters', () => {
		expect(characters).toMatchSchema(charactersSchema);
	});
});

import { AeriDate, Month, Patron } from './AeriDate';

const mapDay = new AeriDate('Tol 56 9999 1:30:15pm');

test("Kir's verbal defence to be on a Saturday", () => {
	expect(new AeriDate(9999, 1, 69).weekday).toBe(6);
});
test('Haaith has to be clown-kin (Zegmaa patron)', () => {
	expect(new AeriDate('Lymonth 66 9784').patron).toBe(Patron.Zegmaa);
});
test('all days have a patron', () => {
	for (let month = 1; month <= 5; month++) {
		for (let day = 1; day <= 73; day++) {
			expect(new AeriDate(9999, month, day).patronName()).toEqual(expect.any(String));
		}
	}
});
test('string parsing (with time)', () => {
	const date = new AeriDate(mapDay);
	expect(date.year).toBe(9999);
	expect(date.month).toBe(Month.Tolmonth);
	expect(date.day).toBe(56);
	expect(date.hours).toBe(13);
	expect(date.minutes).toBe(30);
	expect(date.seconds).toBe(15);
});
test('set year after creating a date', () => {
	const date = new AeriDate(mapDay);
	date.year += 1;
	expect(date).toEqual(new AeriDate('Tol 56 10000 1:30:15pm'));
});
test('add negative year', () => {
	const date = new AeriDate(mapDay);
	date.year -= 9999;
	expect(date).toEqual(new AeriDate('Tol 56 -1 1:30:15pm'));
});
test('set month after creating a date', () => {
	const date = new AeriDate(mapDay);
	date.month += 1;
	expect(date).toEqual(new AeriDate('Var 56 9999 1:30:15pm'));
});
test('set days after creating a date', () => {
	const date = new AeriDate(mapDay);
	date.day += 1;
	expect(date).toEqual(new AeriDate('Tol 57 9999 1:30:15pm'));
});
test('set hours after creating a date', () => {
	const date = new AeriDate(mapDay);
	date.hours += 1;
	expect(date).toEqual(new AeriDate('Tol 56 9999 2:30:15pm'));
});
test('set minutes after creating a date', () => {
	const date = new AeriDate(mapDay);
	date.minutes += 1;
	expect(date).toEqual(new AeriDate('Tol 56 9999 1:31:15pm'));
});
test('set seconds after creating a date', () => {
	const date = new AeriDate(mapDay);
	date.seconds += 1;
	expect(date).toEqual(new AeriDate('Tol 56 9999 1:30:16pm'));
});

import type { AeriDate } from './AeriDate';

export enum Calendar {
	Events,
	Campaign,
	Birthdays
}

export interface CalendarEvent {
	readonly name: string;
	readonly start: AeriDate;
	readonly end: AeriDate;
	readonly calendar: Calendar;
	readonly tags: string[];
	readonly data: Record<string, unknown>;
	readonly fullDay: boolean;
	readonly fullYear: boolean;

	in(date: AeriDate): boolean;
	in(year: number, month?: number, day?: number): boolean;
}

import { AeriDate, Patron } from '$lib/calendars/AeriDate';
import { Mask } from './Mask';

export class Interval {
	public readonly end: AeriDate;
	constructor(
		public readonly start: AeriDate,
		end?: AeriDate | Mask
	) {
		if (end instanceof AeriDate) {
			this.end = end;
		} else if (typeof end == 'number') {
			this.end = new AeriDate(this.start.time + +end);
		} else {
			this.end = new AeriDate(
				this.start.time + +(this.start.time % Mask.DAY === 0 ? Mask.DAY : Mask.TIME)
			);
		}
	}

	public intersects({ start, end }: Interval, yearless = false) {
		return this.start.compare(end, yearless) < 0 && this.end.compare(start, yearless) > 0;
	}

	public contains(date: AeriDate, yearless = false) {
		return this.start.compare(date, yearless) <= 0 && this.end.compare(date, yearless) > 0;
	}

	public toString() {
		return `${this.start.toString()} - ${this.end.toString()}`;
	}
}

export const PATRON_INTERVALS: Map<Patron, Interval> = new Map([
	[Patron.Riid, new Interval(new AeriDate('Tormonth 23 8000'), new AeriDate('Tormonth 54 8000'))],
	[Patron.Refii, new Interval(new AeriDate('Tormonth 54 8000'), new AeriDate('Tolmonth 9 8000'))],
	[Patron.Aiidar, new Interval(new AeriDate('Tolmonth 9 8000'), new AeriDate('Tolmonth 39 8000'))],
	[Patron.Vaator, new Interval(new AeriDate('Tolmonth 39 8000'), new AeriDate('Tolmonth 69 8000'))],
	[Patron.Xuulas, new Interval(new AeriDate('Tolmonth 69 8000'), new AeriDate('Varmonth 27 8000'))],
	[Patron.Taakra, new Interval(new AeriDate('Varmonth 27 8000'), new AeriDate('Varmonth 57 8000'))],
	[Patron.Taana, new Interval(new AeriDate('Varmonth 57 8000'), new AeriDate('Svemonth 15 8000'))],
	[Patron.Nayya, new Interval(new AeriDate('Svemonth 15 8000'), new AeriDate('Svemonth 46 8000'))],
	[Patron.Zriita, new Interval(new AeriDate('Svemonth 46 8000'), new AeriDate('Lymonth 3 8000'))],
	[Patron.Siivra, new Interval(new AeriDate('Lymonth 3 8000'), new AeriDate('Lymonth 34 8000'))],
	[Patron.Soquiis, new Interval(new AeriDate('Lymonth 34 8000'), new AeriDate('Lymonth 64 8000'))],
	[Patron.Zegmaa, new Interval(new AeriDate('Lymonth 64 8000'), new AeriDate('Tormonth 23 8001'))]
]);

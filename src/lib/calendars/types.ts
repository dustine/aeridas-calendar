export type JsonBaseEvent = {
	name: string;
	description?: string;
	location?: string;
};
export type JsonOnceEvent = JsonBaseEvent & {
	start: string;
	time?: string;
	end?: string;
};
export type JsonRepeatEvent = JsonBaseEvent & {
	repeat: string;
	until?: string;
};
export type JsonEvent = JsonOnceEvent | JsonRepeatEvent;
export type JsonCampaignEvent = JsonOnceEvent;
export type JsonCharacter = {
	name: string;
	formerly?: string;
	aka?: string[];

	start?: string;
	startKey?: string | null;
	end?: string;
	endKey?: string | null;

	days?: {
		[day: string]: string;
	};
	events?: {
		[day: string]: string;
	};
};

export function isJsonRepeatEvent(arg: JsonEvent): arg is JsonRepeatEvent {
	return 'repeat' in arg;
}

export const MIN_YEAR = 0;
export const MAX_YEAR = 999999;

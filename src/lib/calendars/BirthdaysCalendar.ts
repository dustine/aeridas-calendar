import { AeriDate } from '$lib/calendars/AeriDate';
import type { JsonCharacter } from '$lib/calendars/types';
import { pluralize, uppercaseAll } from '$lib/helpers';
import { Calendar } from './CalendarEvent';
import { CharacterDayEvent } from './CharacterDayEvent';
import data from './data/characters.json';
import { getMask, Mask } from './Mask';
import type { RepeatYearlyEvent } from './RepeatYearlyEvent';

const characters = data as JsonCharacter[];

function createDaysFromCharacter(character: JsonCharacter): CharacterDayEvent[] {
	function getEventName(key: string) {
		switch (key) {
			default:
				return uppercaseAll(key) + 'day';
		}
	}

	function yearlize(dayMonth: string, year = 1) {
		return (dayMonth.split(' ').length == 2 ? `${dayMonth} ${year}` : dayMonth).trim();
	}

	const events: RepeatYearlyEvent[] = [];
	const name = pluralize(character.name);

	const start = character.start ? new AeriDate(character.start) : undefined;
	const startYear = start?.year;
	const startKey = character.startKey ?? 'birth';
	const until = character.end ? new AeriDate(character.end) : undefined;

	// add birthday
	if (start && character.startKey !== null) {
		let startday: AeriDate | string | undefined = character.days?.[startKey];

		if (startday) {
			startday = yearlize(startday, startYear);
		} else if (character.start && getMask(character.start) <= Mask.DAY) {
			startday = start;
		}

		if (startday) {
			const startDate = new AeriDate(startday);
			events.push(
				new CharacterDayEvent(
					`${name} ${getEventName(startKey)}`,
					startDate,
					until,
					[Calendar.Birthdays, startKey],
					{ character: character, untilMask: character.end ? getMask(character.end) : undefined }
				)
			);
		}
	}

	if (!character.days) return events;

	for (const key in character.days) {
		if (Object.prototype.hasOwnProperty.call(character.days, key)) {
			const day = character.days[key];
			if (!day || key === startKey) continue;

			const repeat = new AeriDate(yearlize(day, startYear));
			// if (isNaN(date.time)) {
			// 	throw new Error(`Invalid day ${key}: ${JSON.stringify(character)}`);
			// }

			events.push(
				new CharacterDayEvent(
					`${name} ${getEventName(key)}`,
					repeat,
					until,
					[Calendar.Birthdays, key],
					{ character: character }
				)
			);
		}
	}

	return events;
}

const BirthdaysCalendar = characters.map((character) => createDaysFromCharacter(character)).flat();
const EndlessBirthdaysCalendar = BirthdaysCalendar.map(
	(event) =>
		new CharacterDayEvent(
			event.name,
			new AeriDate(event.start).setYear(-1, true),
			undefined,
			[event.calendar, ...event.tags],
			{
				...event.data
			}
		)
);

export { BirthdaysCalendar, EndlessBirthdaysCalendar };

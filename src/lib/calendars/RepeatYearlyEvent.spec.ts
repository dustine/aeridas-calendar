import { AeriDate } from './AeriDate';
import { RepeatYearlyEvent } from './RepeatYearlyEvent';

const date = new AeriDate('Tol 56 9999');

test('repeated event should return true on ANY year', () => {
	const event = new RepeatYearlyEvent('Test', date);
	expect(event.in(9999)).toBeTruthy();
	expect(event.in(10000)).toBeTruthy();
});

test('until stops the repeated effect', () => {
	const within = new AeriDate(date).setYear(1, true);
	const outside = new AeriDate(date).setYear(2, true);
	const end = outside.year;
	const event = new RepeatYearlyEvent('Test', date, `${end}`);
	expect(event.in(within)).toBeTruthy();
	expect(event.in(outside)).toBeFalsy();
});

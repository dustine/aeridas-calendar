import { AeriDate } from './AeriDate';
import { Interval } from './Interval';
import { Mask } from './Mask';

const date = new AeriDate('Tol 56 9999');

describe('interval constructors infer relevant end', () => {
	test('timeliness dates add a day', () => {
		const interval = new Interval(date);
		const target = new AeriDate(date).setDay(1, true);
		expect(interval.end).toEqual(target);
	});

	test('other dates add an hour', () => {
		const timefulDate = new AeriDate(date).setHours(1);
		const interval = new Interval(timefulDate);
		const target = new AeriDate(timefulDate).setHours(1, true);
		expect(interval.end).toEqual(target);
	});
});

describe('intersects', () => {
	const getIntersectingInterval = (hours = 0) =>
		new Interval(new AeriDate(date).setHours(hours, true), Mask.TIME * 2);
	const interval = new Interval(date);

	test('no intersection (false)', () => {
		const testInterval = getIntersectingInterval(-3);
		expect(testInterval.intersects(interval)).toBeFalsy();
		expect(interval.intersects(testInterval)).toBeFalsy();
	});

	test('touching (false)', () => {
		const testInterval = getIntersectingInterval(-2);
		expect(testInterval.intersects(interval)).toBeFalsy();
		expect(interval.intersects(testInterval)).toBeFalsy();
	});

	test('half intersection', () => {
		const testInterval = getIntersectingInterval(-1);
		expect(testInterval.intersects(interval)).toBeTruthy();
		expect(interval.intersects(testInterval)).toBeTruthy();
	});

	test('full intersection', () => {
		const testInterval = getIntersectingInterval();
		expect(testInterval.intersects(interval)).toBeTruthy();
		expect(interval.intersects(testInterval)).toBeTruthy();
	});
});

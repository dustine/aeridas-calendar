import { pluralize, uppercaseAll } from '../helpers';
import { AeriDate } from './AeriDate';
import { Calendar } from './CalendarEvent';
import charactersData from './data/characters.json';
import eventsData from './data/events.json';
import { getMask } from './Mask';
import { OnceEvent } from './OnceEvent';
import { RepeatYearlyEvent } from './RepeatYearlyEvent';
import type { JsonCharacter, JsonEvent } from './types';
import { isJsonRepeatEvent } from './types';

const characters = charactersData as JsonCharacter[];
const events = eventsData as JsonEvent[];

function createEventsFromCharacter(character: JsonCharacter): OnceEvent[] {
	function getEventName(key: string) {
		switch (key) {
			case 'turn':
				return 'Turning';
			case 'undeath':
				return 'Death';
			default:
				return uppercaseAll(key);
		}
	}

	const events: OnceEvent[] = [];
	const name = pluralize(character.name);
	const push = (key: string, date: string) => {
		const mask = getMask(date);
		const event = new OnceEvent(
			`${name} ${getEventName(key)}`,
			date,
			mask,
			[Calendar.Events, key],
			{ character: character }
		);
		events.push(event);
	};

	if (character.start) {
		push(character.startKey ?? 'birth', character.start);
	}
	if (character.end) {
		push(character.endKey ?? 'death', character.end);
	}

	if (!character.events) return events;

	for (const key in character.events) {
		if (Object.prototype.hasOwnProperty.call(character.events, key)) {
			const day = character.events[key];
			if (!day) continue;

			const date = new AeriDate(day);
			if (isNaN(date.time)) {
				throw new Error(`Invalid event ${key}: ${JSON.stringify(character)}`);
			}

			const mask = getMask(day);
			events.push(
				new OnceEvent(`${name} ${getEventName(key)}`, date, mask, [Calendar.Events, key], {
					character: character
				})
			);
		}
	}

	return events;
}

const EventsCalendar = events
	.map((value: JsonEvent) => {
		if (isJsonRepeatEvent(value)) {
			return new RepeatYearlyEvent(value.name, value.repeat, value.until, undefined, {
				description: value.description,
				location: value.location,
				untilMask: value.until ? getMask(value.until) : undefined
			});
		} else {
			const mask = getMask(value.start);
			return new OnceEvent(value.name, value.start, value.end ?? mask, undefined, {
				description: value.description,
				location: value.location,
				time: value.time
			});
		}
	})
	.concat(
		characters.map((character: JsonCharacter) => createEventsFromCharacter(character)).flat()
	);

const UniqueEventsCalendar = EventsCalendar.filter(
	(event): event is OnceEvent => !('repeats' in event)
);

export { EventsCalendar, UniqueEventsCalendar };

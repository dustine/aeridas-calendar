import { AeriDate } from './AeriDate';
import type { CalendarEvent } from './CalendarEvent';
import { Calendar } from './CalendarEvent';
import { Mask } from './Mask';
import { MAX_YEAR } from './types';

export class RepeatYearlyEvent implements CalendarEvent {
	public readonly start: AeriDate;
	public readonly end: AeriDate;
	public readonly calendar: Calendar;
	public readonly tags: string[];
	public readonly data: Record<string, unknown>;
	public readonly fullDay = true;
	public readonly fullYear = false;

	public readonly until?: AeriDate;
	public readonly repeats = 'yearly';

	constructor(
		public readonly name: string,
		repeat: string | AeriDate,
		until?: string | AeriDate,
		tags: [Calendar, ...string[]] = [Calendar.Events],
		data: Record<string, unknown> = {}
	) {
		this.start = new AeriDate(
			repeat instanceof AeriDate ? repeat : `${repeat} ${repeat.split(' ').length == 2 ? '1' : ''}`
		)
			.setHours(0)
			.setMinutes(0)
			.setSeconds(0);
		this.end = new AeriDate(this.start.time + +Mask.DAY);

		[this.calendar, ...this.tags] = tags;
		this.data = Object.fromEntries(Object.entries(data).filter(([, value]) => value !== undefined));

		if (until) {
			if (until instanceof AeriDate) {
				this.until = until;
			} else {
				this.until = new AeriDate(until);
			}
		}
	}

	public in(target: AeriDate | number, month?: number, day?: number): boolean {
		if (!(target instanceof AeriDate)) {
			if (!month && !day && !this.until) return target >= this.start.year;
			target = new AeriDate(target, month ?? this.start.month, day ?? this.start.day);
		}

		return (
			this.start.compare(target) <= 0 &&
			this.start.compare(target, true) <= 0 &&
			this.end.compare(target, true) > 0 &&
			(!this.until || this.until.compare(target) > 0)
		);
	}

	public *instances(until = this.until?.year || MAX_YEAR) {
		let year = this.start.year;
		const month = this.start.month;
		const day = this.start.day;
		while (year <= until) {
			if (!this.in(year, month, day)) return;
			yield new AeriDate(year, month, day);
			++year;
		}
	}
}

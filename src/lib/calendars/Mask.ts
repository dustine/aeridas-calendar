export enum Mask {
	YEAR = 5 * 73 * 24 * 60 * 60,
	MONTH = 73 * 24 * 60 * 60,
	DAY = 24 * 60 * 60,
	TIME = 60 * 60 // one hour
}

export function getMask(date: string): number {
	switch (date?.split(' ').length) {
		case 1:
			return Mask.YEAR;
		case 2:
			return Mask.MONTH;
		case 3:
			return Mask.DAY;
		default:
			return Mask.TIME;
	}
}

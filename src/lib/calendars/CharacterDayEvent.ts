import type { AeriDate } from './AeriDate';
import { Calendar } from './CalendarEvent';
import { RepeatYearlyEvent } from './RepeatYearlyEvent';

export class CharacterDayEvent extends RepeatYearlyEvent {
	constructor(
		name: string,
		repeat: string | AeriDate,
		until?: string | AeriDate,
		tags: [Calendar, ...string[]] = [Calendar.Events],
		data: Record<string, unknown> = {}
	) {
		super(name, repeat, until, tags, data);
		this.start.setYear(+1, true);
		this.end.setYear(+1, true);
	}
}

import { MAX_YEAR, MIN_YEAR } from './types';

export const SECONDS_IN_MINUTE = 60;
export const SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE;
export const SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR;
export const SECONDS_IN_MONTH = 73 * SECONDS_IN_DAY;
export const SECONDS_IN_YEAR = 5 * SECONDS_IN_MONTH;

export enum Weekday {
	Sunday,
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday
}
export enum Month {
	Tormonth = 1,
	Tolmonth,
	Varmonth,
	Svemonth,
	Lymonth
}
export enum MonthSymbol {
	'⭘' = 1,
	'🜂',
	'🜁',
	'🜄',
	'🜃'
}
const MONTH_NAMES = [Month[1], Month[2], Month[3], Month[4], Month[5]].map((name) =>
	name.toLowerCase()
);
export enum Patron {
	Aiidar,
	Vaator,
	Xuulas,
	Taakra,
	Taana,
	Nayya,
	Zriita,
	Siivra,
	Soquiis,
	Zegmaa,
	Riid,
	Refii
}
export const PATRON_NAMES = [
	Patron[0],
	Patron[1],
	Patron[2],
	Patron[3],
	Patron[4],
	Patron[5],
	Patron[6],
	Patron[7],
	Patron[8],
	Patron[9],
	Patron[10],
	Patron[11]
] as const;
export enum PatronSymbol {
	'♈',
	'♉',
	'♊',
	'♋',
	'♌',
	'♍',
	'♎',
	'♏',
	'♐',
	'♑',
	'♒',
	'♓'
}
// TODO: moon phases (first full moon on Tormonth 15th?)
// TODO: easter egg where it's Winter for a single snowy day
/** //TODO: yearly zodiac, 7 year cycle where:
 * - Human:     Dog
 * - Dwarves:   Guinea Pig
 * - Elves:     Deer
 * - Gnomes:
 * - Halflings:
 * - Tieflings: Goat
 * - Orcs:      Cow
 */

export class AeriDate {
	private _time: number;
	private get yearlessTime(): number {
		return ((this.time % SECONDS_IN_YEAR) + SECONDS_IN_YEAR) % SECONDS_IN_YEAR;
	}

	public set time(value: number) {
		// don't allow unsafe values
		this._time =
			value < Number.MIN_SAFE_INTEGER ||
			value > Number.MAX_SAFE_INTEGER ||
			value < MIN_YEAR * SECONDS_IN_YEAR ||
			value > MAX_YEAR * SECONDS_IN_YEAR - 1
				? NaN
				: Math.round(value);
	}
	public get time(): number {
		return this._time;
	}
	public get year(): number {
		return Math.floor(this.time / SECONDS_IN_YEAR) + (this.time < 0 ? 0 : 1);
	}
	public set year(value: number) {
		this.time = this.time + (value - this.year) * SECONDS_IN_YEAR;
	}
	public get month(): number {
		return Math.floor((this.yearlessTime % SECONDS_IN_YEAR) / SECONDS_IN_MONTH) + 1;
	}
	public set month(value: number) {
		this.time = this.time + (value - this.month) * SECONDS_IN_MONTH;
	}
	public get day(): number {
		return Math.floor((this.yearlessTime % SECONDS_IN_MONTH) / SECONDS_IN_DAY) + 1;
	}
	public set day(value: number) {
		this.time = this.time + (value - this.day) * SECONDS_IN_DAY;
	}
	public get hours(): number {
		return Math.floor((this.yearlessTime % SECONDS_IN_DAY) / SECONDS_IN_HOUR);
	}
	public set hours(value: number) {
		this.time = this.time + (value - this.hours) * SECONDS_IN_HOUR;
	}
	public get minutes(): number {
		return Math.floor((this.yearlessTime % SECONDS_IN_HOUR) / SECONDS_IN_MINUTE);
	}
	public set minutes(value: number) {
		this.time = this.time + (value - this.minutes) * SECONDS_IN_MINUTE;
	}
	public get seconds(): number {
		return this.yearlessTime % SECONDS_IN_MINUTE;
	}
	public set seconds(value: number) {
		this.time = this.time + (value - this.seconds);
	}
	public get weekday(): number {
		return (((Math.floor(this.time / SECONDS_IN_DAY) + 6) % 7) + 7) % 7;
	}
	public get patron(): Patron {
		switch (this.month) {
			case Month.Tormonth:
				if (this.day < 23) return Patron.Zegmaa;
				if (this.day < 54) return Patron.Riid;
				return Patron.Refii;
			case Month.Tolmonth:
				if (this.day < 9) return Patron.Refii;
				if (this.day < 39) return Patron.Aiidar;
				if (this.day < 69) return Patron.Vaator;
				return Patron.Xuulas;
			case Month.Varmonth:
				if (this.day < 27) return Patron.Xuulas;
				if (this.day < 57) return Patron.Vaator;
				return Patron.Taana;
			case Month.Svemonth:
				if (this.day < 15) return Patron.Taana;
				if (this.day < 46) return Patron.Nayya;
				return Patron.Zriita;
			case Month.Lymonth:
				if (this.day < 3) return Patron.Zriita;
				if (this.day < 34) return Patron.Siivra;
				if (this.day < 64) return Patron.Soquiis;
				return Patron.Zegmaa;
			default:
				throw new Error('Invalid Date');
		}
	}

	public monthName(format: 'long' | 'short' = 'long'): string {
		switch (format) {
			case 'long':
				return Month[this.month];
			case 'short':
				return Month[this.month].replace('month', '');
		}
	}
	public weekdayName(): string {
		return Weekday[this.weekday];
	}
	public patronName(): string {
		return Patron[this.patron];
	}
	public patronSymbol(): string {
		return PatronSymbol[this.patron];
	}

	public setYear(value: number, relative = true): this {
		this.year = relative ? this.year + value : value;
		return this;
	}
	public setMonth(value: number, relative = true): this {
		this.month = relative ? this.month + value : value;
		return this;
	}
	public setDay(value: number, relative = false): this {
		this.day = relative ? this.day + value : value;
		return this;
	}
	public setHours(value: number, relative = false): this {
		this.hours = relative ? this.hours + value : value;
		return this;
	}
	public setMinutes(value: number, relative = false): this {
		this.minutes = relative ? this.minutes + value : value;
		return this;
	}
	public setSeconds(value: number, relative = false): this {
		this.seconds = relative ? this.seconds + value : value;
		return this;
	}

	public static firstOfMonth(year: number, month: number) {
		// TODO this could be done arithmetically
		return new AeriDate(year, month).weekday;
	}

	public static parse(value: string): number {
		// (([Tor 31 ]9999 [00[:00[:00]][am|pm]])
		let time = 0;
		let parse = value.trim().toLowerCase();

		// match the time, [0+[:0+[:0+]][am|pm]])
		const timeRegex = /(?:T|\s+)(\d+):(\d+)?(?::(\d+))?\s*(am|pm)?/;
		const timeParse = timeRegex.exec(parse);
		if (timeParse) {
			parse = parse.replace(timeParse[0], '').trim();
			let hour = +timeParse[1];
			if (timeParse[4]) {
				// meridian
				hour = (hour % 12) + (timeParse[4] == 'pm' ? 12 : 0);
			}
			time += hour * SECONDS_IN_HOUR;
			time += +(timeParse[2] ?? '') * SECONDS_IN_MINUTE;
			time += +(timeParse[3] ?? '');
		}

		const dateParse = parse.split(' ');
		if (dateParse.length == 1) {
			// "9999"
			time = (+dateParse[0] - 1) * SECONDS_IN_YEAR;
		} else {
			// [Tor 31] 9999 / [31 Tor] 9999
			// check which month starts with the key, To is ambiguous but welp
			const getMonth = (value: string) =>
				MONTH_NAMES.findIndex((month) => month.startsWith(value)) + 1 || NaN;

			if (isNaN(+dateParse[0])) {
				time += (getMonth(dateParse[0]) - 1) * SECONDS_IN_MONTH;
				if (dateParse[2]) {
					time += (+dateParse[1] - 1) * SECONDS_IN_DAY;
					const year = +dateParse[2];
					time += (year - (year < 0 ? 0 : 1)) * SECONDS_IN_YEAR;
				} else {
					const year = +dateParse[1];
					time += (year - (year < 0 ? 0 : 1)) * SECONDS_IN_YEAR;
				}
			} else if (isNaN(+dateParse[1])) {
				time += (+dateParse[0] - 1) * SECONDS_IN_DAY;
				time += (getMonth(dateParse[1]) - 1) * SECONDS_IN_MONTH;
				const year = +dateParse[2];
				time += (year - (year < 0 ? 0 : 1)) * SECONDS_IN_YEAR;
			} else {
				time = NaN;
			}
		}
		return time;
	}

	constructor(value: string | number | AeriDate);
	constructor(
		year: number,
		month: number,
		date?: number,
		hours?: number,
		minutes?: number,
		seconds?: number
	);
	constructor(
		year: number | string | AeriDate,
		month?: number,
		date = 1,
		hours = 0,
		minutes = 0,
		seconds = 0
	) {
		if (year instanceof AeriDate) {
			this._time = year.time;
		} else if (typeof year === 'string') {
			this._time = AeriDate.parse(year);
		} else if (typeof month === 'undefined') {
			this._time = year;
		} else {
			this._time =
				(year - (year < 0 ? 0 : 1)) * SECONDS_IN_YEAR +
				(month - 1) * SECONDS_IN_MONTH +
				(date - 1) * SECONDS_IN_DAY +
				hours * SECONDS_IN_HOUR +
				minutes * SECONDS_IN_MINUTE +
				seconds;
		}
		// this runs the boundary check + rounder on the setter once
		this.time = this._time;
	}

	public compare(rhs: AeriDate, yearless = false): number {
		return yearless ? this.yearlessTime - rhs.yearlessTime : this.time - rhs.time;
	}

	public toString(): string {
		if (isNaN(this.time)) return 'Invalid Date';

		return [
			this.weekdayName().slice(0, 3),
			this.monthName().replace('month', ''),
			this.day,
			this.year,
			[this.hours, this.minutes, this.seconds]
				.map((value) => value.toString().padStart(2, '0'))
				.join(':')
		].join(' ');
	}
}

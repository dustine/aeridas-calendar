import { AeriDate } from './AeriDate';
import type { CalendarEvent } from './CalendarEvent';
import { Calendar } from './CalendarEvent';
import { Interval } from './Interval';
import { Mask } from './Mask';

export class OnceEvent implements CalendarEvent {
	public readonly interval: Interval;
	public readonly calendar: Calendar;
	public readonly tags: string[];
	public readonly data: Record<string, unknown>;

	public get start() {
		return this.interval.start;
	}

	public get end() {
		return this.interval.end;
	}

	public get fullDay() {
		return this.start.time % Mask.DAY === 0 && this.end.time % Mask.DAY === 0;
	}

	public get fullYear() {
		return this.start.time % Mask.MONTH === 0 && this.end.time % Mask.MONTH === 0;
	}

	constructor(
		public readonly name: string,
		start: string | AeriDate,
		end?: string | AeriDate | number,
		tags: [Calendar, ...string[]] = [Calendar.Events],
		data: Record<string, unknown> = {}
	) {
		this.interval = new Interval(
			typeof start === 'string' ? new AeriDate(start) : start,
			typeof end === 'string' ? new AeriDate(end) : end
		);
		[this.calendar, ...this.tags] = tags;
		this.data = Object.fromEntries(Object.entries(data).filter(([, value]) => value !== undefined));
	}

	public in(target: AeriDate | number, month?: number, day?: number): boolean {
		let targetInterval: Interval;
		if (typeof target === 'number') {
			targetInterval = new Interval(
				new AeriDate(target, month ?? 1, day ?? 1),
				+(!month ? Mask.YEAR : !day ? Mask.MONTH : Mask.DAY)
			);
		} else {
			targetInterval = new Interval(target);
		}
		return this.interval.intersects(targetInterval);
	}
}

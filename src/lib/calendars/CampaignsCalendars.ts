import { Calendar } from './CalendarEvent';
import shards from './data/shards.json';
import heist from './data/heist.json';
import { OnceEvent } from './OnceEvent';
import type { JsonCampaignEvent } from './types';

const campaigns = { shards, heist };

export const CAMPAIGNS = Object.keys(campaigns);
export const CAMPAIGN_NAMES = Object.fromEntries(
	[...Object.entries(campaigns)].map(([key, { name }]) => [key, name])
);
export function isCampaign(slug: string): slug is keyof typeof campaigns {
	return slug in campaigns;
}

const CampaignsCalendars = new Map<string, OnceEvent[]>();
for (const key in campaigns) {
	if (isCampaign(key)) {
		CampaignsCalendars.set(
			key,
			campaigns[key].events.map(
				(value: JsonCampaignEvent) =>
					new OnceEvent(value.name, value.start, value.end, [Calendar.Campaign, key], {
						description: value.description,
						location: value.location,
						time: value.time
					})
			)
		);
	}
}

export { CampaignsCalendars };

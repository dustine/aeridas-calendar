import { AeriDate, Month } from './AeriDate';
import { OnceEvent } from './OnceEvent';

const eventDate = new AeriDate('Ly 73 9999');
const event = new OnceEvent('Test', eventDate);
const nextDay = new AeriDate(eventDate).setDay(1, true);

test('events are contained within their date', () => {
	expect(event.start.year).toBe(9999);
	expect(event.end).toEqual(nextDay);
});

test('events should be in() within their date components', () => {
	expect(event.in(9998)).toBeFalsy();
	expect(event.in(9999, Month.Svemonth)).toBeFalsy();
	expect(event.in(9999, Month.Lymonth, 72)).toBeFalsy();
	expect(event.in(9999)).toBeTruthy();
	expect(event.in(9999, Month.Lymonth)).toBeTruthy();
	expect(event.in(9999, Month.Lymonth, 73)).toBeTruthy();
	expect(event.in(10000)).toBeFalsy();
});

test('should be in() in start, not inclusive of end', () => {
	const prevDay = new AeriDate(eventDate).setDay(-1, true);
	expect(event.in(prevDay)).toBeFalsy();
	expect(event.in(eventDate)).toBeTruthy();
	expect(event.in(nextDay)).toBeFalsy();
});

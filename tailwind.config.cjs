/** @type {import('tailwindcss').Config}*/
module.exports = {
	darkMode: 'class',
	content: ['./src/**/*.{js,html,svelte,ts}'],
	plugins: [require('@tailwindcss/typography')],
	theme: {
		extend: {}
	}
};
